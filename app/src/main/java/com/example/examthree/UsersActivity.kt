package com.example.examthree

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.examthree.databinding.ActivityUserBinding

class UsersActivity : AppCompatActivity() {

    lateinit var binding: ActivityUserBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val datasource = DataSource.loadUsers()
        binding.userRecyclerview.adapter = UserAdapter(this,datasource)
        binding.userRecyclerview.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL,false)
    }
}