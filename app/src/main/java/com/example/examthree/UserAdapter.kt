package com.example.examthree

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class UserAdapter(val context:Context,val listOfUsers:MutableList<User>):RecyclerView.Adapter<UserAdapter.UserViewHolder>() {


    inner class UserViewHolder(item: View):RecyclerView.ViewHolder(item)
    {
       val firstName = item.findViewById<TextView>(R.id.tv_name)
        val lastname = item.findViewById<TextView>(R.id.tv_last_name)
        val email = item.findViewById<TextView>(R.id.email)
        val updateUser = item.findViewById<ImageButton>(R.id.update_user)
        val deleteUser = item.findViewById<ImageButton>(R.id.delete_user)


        fun bindUpdateListener()
        {
            updateUser.setOnClickListener {
                val intent = Intent(context, UserActivity::class.java)
                context.startActivity(intent)
            }

        }

        fun bindDeleteListener(holder:UserViewHolder)
        {
            deleteUser.setOnClickListener {
               deleteUser(holder)
            }
        }

        fun deleteUser(holder:UserViewHolder)
        {
            val newPosition: Int = holder.adapterPosition
            listOfUsers.removeAt(newPosition)
            notifyItemRemoved(newPosition)
            notifyItemRangeChanged(newPosition, listOfUsers.size)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.user_item, parent, false)

        return UserViewHolder(view)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val item = listOfUsers[position]
        holder.firstName.setText(item.firstName)
        holder.lastname.setText(item.lastName)
        holder.email.setText(item.email)

        holder.bindUpdateListener()

        holder.bindDeleteListener(holder)

    }

    override fun getItemCount(): Int {
       return listOfUsers.size
    }

}